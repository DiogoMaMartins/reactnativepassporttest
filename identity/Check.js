import React from 'react';
import { StyleSheet, Text, View,TextInput,Button,AsyncStorage} from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';

import TakePicture from './TakePicture.js';

export default class Check extends React.Component {
  state = {
    hasCameraPermission: null,
    data:null,
    firstName:null,
    lastName:null,
    country:null,
    ready:false,
  }

  async componentDidMount() {
    let removeItems = ['lastname','firstname','gender','birthDate','expiryDate','country','cardNumber','nationalNumber']
    AsyncStorage.multiRemove(removeItems, (err) => {
      if(err)
      console.log(err)

      console.log("removed",removeItems)
    });
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
    
    let lastName = await AsyncStorage.getItem('lastname')
    let firstName = await AsyncStorage.getItem('firstname')
    let gender = await AsyncStorage.getItem('gender') 
    let birthDate = await AsyncStorage.getItem('birthDate') 
    let expiryDate = await AsyncStorage.getItem('expiryDate') 
    let country = await AsyncStorage.getItem('country') 
    let cardNumber = await AsyncStorage.getItem('cardNumber') 
    let nationalNumber = await AsyncStorage.getItem('nationalNumber')
    this.setState({
      firstName,
      lastName,
      gender,
      birthDate,
      expiryDate,
      country,
      cardNumber,
      nationalNumber
    })
    }

    
 
  render() {
    const { navigate } = this.props.navigation;
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }

    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View style={{ flex: 1 }}>
 
        <View style={{ flex:2 }}>

          <BarCodeScanner
            onBarCodeScanned={this.handleBarCodeScanned}
            style={styles.barCodeScannerCamera}
          />
        
          
        </View>

        <Button
         title="Test OCR"
        onPress={() =>
              navigate('TakePicture')
            }
            />
        {/*<View style={{flex:1}}>*/}
        <Text>BarCode:{this.state.data}</Text>

        <View style={styles.input} >
          <Text>National Register</Text>
          <TextInput value={this.state.nationalNumber}/>
        </View>

          <View style={styles.input} >
          <Text>Birth Date</Text>
          <Text>{this.state.birthDate}</Text>
          </View>

          <View style={styles.input} >
          <Text>First Name</Text>
          <Text>{this.state.firstName}</Text>
          </View>

          <View style={styles.input} >
          <Text>Last Name</Text>
          <Text>{this.state.lastName}</Text>
          </View>

          <View style={styles.input} >
          <Text>Country</Text>
          <Text>{this.state.country}</Text>
          </View>

          <View style={styles.input} >
          <Text>gender</Text>
          <Text>{this.state.gender}</Text>
          </View>

          <View style={styles.input} >
          <Text>cardNumber</Text>
          <Text>{this.state.cardNumber}</Text>
          </View>

          <View style={styles.input} >
          <Text>expiryDate</Text>
          <Text>{this.state.expiryDate}</Text>
          </View>

    
    {/*</View>*/}
    </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    let birthDate = data.substring(0,6).match(/.{1,2}/g).join(".")
    let restNumberAfterRemoveBirthDate = data.substring(6,11).match(/.{1,3}/g).join(".")
    let nationalNumber = birthDate +"-"+restNumberAfterRemoveBirthDate;
    this.setState({
      data,
      birthDate,
      restNumberAfterRemoveBirthDate,
      nationalNumber,
      ready:true
    })

  }
}

const styles = StyleSheet.create({
  barCodeScannerCamera: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height:100,
    display:'none'
  },
  input: {
    borderColor: '#4D3577',
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 12,
    borderRadius: 9,
    justifyContent: 'center',
    margin:10
  }
  
})