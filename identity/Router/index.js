import {createStackNavigator, createAppContainer } from 'react-navigation';
import Check from '../Check.js';
import TakePicture from '../TakePicture.js';
//import TextDetection from '../TextDetection.js';


const AppNavigator = createStackNavigator({
    /*TextDetection:{
        screen:TextDetection,
        navigationOptions:{
            header:null,
            title: 'TextDetection'
        }
    },*/
    /*Check: {
        screen: Check,
        navigationOptions: {
            header: null,
            title: 'Check'
        },
    
    },*/
    TakePicture:{
        screen:TakePicture,
        navigationOptions:{
            header:null,
            title: 'TakePicture'
        }
    },
    Check: {
        screen: Check,
        navigationOptions: {
            header: null,
            title: 'Check'
        },
    
    }


  })

  export default createAppContainer(AppNavigator);