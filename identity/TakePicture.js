import React, { Component } from 'react';
import { Text, View, StyleSheet,Button, TouchableOpacity,AsyncStorage} from 'react-native';
import Expo, { Constants, Permissions, Camera, MediaLibrary } from 'expo';
import { Ionicons } from '@expo/vector-icons';

let firstname = "";
let lastname = "";
export default class TakePicture extends Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

takePicture = async () => {
  let foto;     
  if (this.camera) {
     console.log('Taking photo');
     const options = { quality: 1, base64: true, fixOrientation: true, 
     exif: true};
     await this.camera.takePictureAsync(options).then(photo => {
        photo.exif.Orientation = 1;            
         foto = photo.uri;   
         console.log(foto)         
         }) 
           try{
            let uriParts = foto.split('.');
            let fileType = uriParts[uriParts.length - 1];
           /* if (fileType === 'jpg')
                fileType = 'jpeg'*/
            let formData = new FormData();
            let config = {
                uri: foto,
                type: "img/" + fileType,
                name: "test"
            }
            formData.append('img', config);

            await fetch(`http://192.168.0.106:8000/api/image`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
                body: formData,
            })
            .then(function(response){
              console.log("res",response)
              console.log("im response",response._bodyInit)

              let lastRowBelgiumId = response._bodyInit.length - 30;

              let removeSpecialCaracters = response._bodyInit.substring(lastRowBelgiumId, response._bodyInit.length).split("<")

              takeWords = removeSpecialCaracters.filter(word => word !== '')
               firstname = takeWords[takeWords.length -1]
               lastname = takeWords[0]
               AsyncStorage.setItem('lastname',lastname) 
              
               AsyncStorage.setItem('firstname',firstname)  
               let  secondRowBelgiumId = response._bodyInit.substring(lastRowBelgiumId - 31,lastRowBelgiumId)
               let birthDate = secondRowBelgiumId.substring(0,6).match(/.{1,2}/g).join(".");
               let gender = secondRowBelgiumId[7] 
               let expiryDate = secondRowBelgiumId.substring(8,14).match(/.{1,2}/g).join(".");
               let country = secondRowBelgiumId.substring(15,18)
               let data = secondRowBelgiumId.substring(16,26)

               let birthDates = data.substring(0,6).match(/.{1,2}/g).join(".")
               let restNumberAfterRemoveBirthDate = data.substring(6,11).match(/.{1,3}/g).join(".")
               let nationalNumber = birthDates +"-"+restNumberAfterRemoveBirthDate;

               AsyncStorage.setItem('gender',gender) 
               AsyncStorage.setItem('birthDate',birthDate) 
               AsyncStorage.setItem('expiryDate',expiryDate) 
               AsyncStorage.setItem('country',country)
               AsyncStorage.setItem('nationalNumber',nationalNumber) 
              
              let  firstRowBelgiumId = response._bodyInit.substring(response._bodyInit.length - 90,response._bodyInit.length - 60);
              let cardNumber = firstRowBelgiumId.substring(3,12) + firstRowBelgiumId[13]

              AsyncStorage.setItem('cardNumber',cardNumber) 
            })
          }catch(err){
            throw err
          }    

   }
  }



  render() {
    const { navigate } = this.props.navigation;
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera style={{ flex: 1 }}
      ref={ (ref) => {this.camera = ref} }
      type={this.state.type}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={{
                  flex: 0.1,
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                }}
                onPress={() => {
                  this.setState({
                    type: this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  });
                }}>
                <Text
                  style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                  {' '}Flip{' '}
                </Text>
              </TouchableOpacity>
              

              <TouchableOpacity
                                style={{
                                    flex: 0.1,
                                    alignSelf: 'flex-end',
                                    alignItems: 'center',
                                }}
                                onPress={() => this.takePicture()}>
                                <Ionicons style={{ marginBottom: 30, }} name="ios-camera" size={48} color="white" />
                            </TouchableOpacity>
                          
            </View>
          </Camera>
          <Button
                            style={{ marginBottom: 30,marginLeft:100,position:'absolute' }}
         title="return"
        onPress={() =>
              navigate('Check')
            }
            />
        </View>
      );
    }
  }
}
